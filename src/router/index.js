import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Buy from '../views/Buy.vue'
import Movie from '../views/MoviesView.vue'
import History from '../views/HistoryV.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/buy/:idMovie',
    name: 'buy',
    component: Buy,
    props: true
  },
  {
    path: '/movie',
    name: 'movie',
    component: Movie,
    props: true
  },
  {
    path: '/history',
    name: 'history',
    component: History
  }
]

const router = new VueRouter({
  routes
})

export default router
